interface AutowireCategories
{
  [category: string]: string[]
}

declare const dynamic_classes: AutowireCategories
